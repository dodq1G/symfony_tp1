<?php

namespace App\Controller;


use App\Entity\Auteur;
use App\Form\AuteurFormType;
use App\Repository\AuteurRepository;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class AuteurController extends AbstractController
{
    #[Route('/auteurs', name: 'auteurs_list')]
    public function list(AuteurRepository $auteurRepository): Response
    {
        $auteurs = $auteurRepository->findAll();

        return $this->render('auteur/list.html.twig', [
            'auteurs' => $auteurs,
        ]);
    }

    #[Route('/auteur/{id}', name: 'auteur_id', requirements: ['id' => '\d+'])]
    public function id(int $id, ArticleRepository $articleRepository, AuteurRepository $auteurRepository): Response
    {
        $auteur = $auteurRepository->find($id);
        $articles = $articleRepository->findByidAuteur($auteur->getId());

        return $this->render('auteur/id.html.twig', [
            'auteur' => $auteur,
            'articles' => $articles
        ]);
    }

    #[Route('/auteur/add', name: 'add_auteur')]
    public function addAuteur(Request $request, EntityManagerInterface $entityManager): Response
    {
        $auteur = new Auteur();
        $form = $this->createForm(AuteurFormType::class, $auteur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($auteur);
            $entityManager->flush();
            $this->addFlash('success', 'Auteur ajouté avec succès.');
            return $this->redirectToRoute('auteurs_list');
        }

        return $this->render('auteur/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/auteur/remove/{id}', name: 'remove_auteur')]
    public function removeAuteur(int $id, EntityManagerInterface $entityManager, AuteurRepository $auteurRepository): Response
    {
        $auteur = $auteurRepository->find($id);

        $entityManager->remove($auteur);
        $entityManager->flush();
        $this->addFlash('success', 'Auteur supprimé avec succès.');

        return $this->redirectToRoute('auteurs_list');
    }

    #[Route('/auteur/modify/{id}', name: 'modify_auteur')]
    public function modifyAuteur(int $id, Request $request, EntityManagerInterface $entityManager, AuteurRepository $auteurRepository): Response
    {
        $auteur = $auteurRepository->find($id);
    
        $form = $this->createForm(AuteurFormType::class, $auteur);
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', 'Auteur modifié avec succès.');
            return $this->redirectToRoute('auteurs_list');
        }
    
        return $this->render('auteur/modify.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    
}
