<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\AuteurRepository;
use App\Repository\TagRepository;
use App\Repository\ArticleRepository;

class MainController extends AbstractController
{
    #[Route('/', name: 'main')]
    public function main(AuteurRepository $auteurRepository, ArticleRepository $articleRepository, TagRepository $tagRepository): Response
    {
        $auteurs = $auteurRepository->findAll();
        $articles = $articleRepository->findAll();
        $tags = $tagRepository->findAll();

        return $this->render('main/main.html.twig', [
            'auteurs' => $auteurs,
            'articles' => $articles,
            'tags' => $tags
        ]);
    }
}